#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

include /usr/share/dpkg/pkg-info.mk

# cf. https://wiki.debian.org/ToolChain/LTO and https://github.com/golang/go/issues/45275
export DEB_BUILD_MAINT_OPTIONS=optimize=-lto

export GO111MODULE=off

export DH_GOLANG_GO_GENERATE := 1

export DH_GOLANG_INSTALL_EXTRA := $(wildcard libpod/*/testdata/* libpod/*/cni/*/*/*)
export DH_GOLANG_EXCLUDES := dependencies test/e2e contrib/perftest test/utils test/endpoint hack/podman-registry-go pkg/bindings/test

export HOME=$(CURDIR)/debian/tmp

## https://podman.io/getting-started/installation#build-tags
BUILDTAGS := apparmor,ostree,seccomp,selinux,systemd
# containers_image_openpgp

%:
	dh $@ --buildsystem=golang --with=golang,bash-completion --builddirectory=_output

override_dh_auto_build:
	$(MAKE) docs docker-docs

#	LDFLAGS_PODMAN="-X main.gitCommit=$(GIT_COMMIT)"
	dh_auto_build -v  --builddirectory=_output -- -tags "$(BUILDTAGS)" \
          -ldflags "-X main.buildInfo=$(DEB_VERSION)"

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	# make test binaries available where the tests expect them
	ln -sv  ../../../../bin  _output/src/github.com/containers/podman/bin
	mkdir -p -m700 $(CURDIR)/debian/tmp-home/.cache
	mkdir -p -m700 $(CURDIR)/debian/tmp-home/.config
	mkdir -p -m700 $(CURDIR)/debian/tmp-home/.local/share
	mkdir -p -m700 $(CURDIR)/debian/tmp-home/run
	env \
	  HOME=$(CURDIR)/debian/tmp-home/.cache \
	  XDG_CACHE_HOME=$(CURDIR)/debian/tmp-home/.cache \
	  XDG_CONFIG_HOME=$(CURDIR)/debian/tmp-home/.config \
	  XDG_DATA_HOME=$(CURDIR)/debian/tmp-home/.local/share \
	  XDG_RUNTIME_DIR=$(CURDIR)/debian/tmp-home/run \
	  PATH="$(CURDIR)/_output/bin:$$PATH" \
	  DH_GOLANG_EXCLUDES="$${DH_GOLANG_EXCLUDES} libpod/network/cni libpod/lock/file libpod/hack/podman-registry-go libpod/pkg/bindings/test podman/cmd/podman/common" \
	dh_auto_test -v --max-parallel=2 -- -tags "$(BUILDTAGS)"
	rm -rf $(CURDIR)/debian/tmp-home
endif

override_dh_auto_install:
	dh_auto_install --destdir=debian/tmp --buildsystem=golang --builddirectory=_output
	# avoid exposing unnecessary vendor libraries
	rm -rfv debian/tmp/usr/share/gocode/src/github.com/containers/libpod/vendor
	# generate and install systemd unit files
	make DESTDIR=debian/tmp PREFIX=/ LIBEXECDIR=/lib/ install.systemd

# Disable dh_missing
override_dh_missing:

override_dh_installsystemd:
	dh_installsystemd
	dh_installsystemd --name=podman-auto-update
	dh_installsystemd --name=podman-restart
